﻿using System;
using System.Collections.Generic;
using System.Text;


namespace InheritanceTask
{
    //TODO: Create public class 'Company' here
    public class Company
    {
        //filed Employee[]
        private Employee[] employees;

        public Company(int n)
        {
            employees= new Employee[n]; 
        }
        public Company(Employee[] employees)
        {
            this.employees = employees;
        }
        public void GiveEverbodyBonus(Decimal companyBonus)
        {
            for(int i=0;i<employees.Length;i++)
            {
                //Employee employee = employees[i];
                //employee.SetBonus(companyBonus);
                employees[i].SetBonus(companyBonus);
            }
        }
        public decimal TotalToPay()
        {
            decimal total = 0;
            for(int i = 0; i < employees.Length-1; i++)
            {
                total +=employees[i].ToPay();
            }
            return total;
        }
        public String NameMaxSalary()
        {
            String maxName = employees[0].Name; ;decimal sal=0;
            decimal max = employees[0].ToPay();
            for(int i = 0; i < employees.Length; i++)
            {
                sal = employees[i].ToPay();
                if (sal > max) { max = sal; maxName = employees[i].Name; }
            }
                return maxName;
        }
    }

    //TODO: Define private field that is array of employees: 'employees'

    //TODO: Define constructor that gets array of employees, and assign them to its field

    //TODO: Define public method 'GiveEverbodyBonus' with parameter 'companyBonus', that set basic bonus to every employee in the company
    //TODO: Define public method 'TotalToPay', that returns the total salary + bonus of all employees of the company 
    //TODO: Define public method 'NameMaxSalary', that returns emloyee’s name who has maximum salary + bonus in the company 
}
